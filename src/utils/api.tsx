export const hostURL = 'https://5c3db915a9d04f0014a98a79.mockapi.io';
export const campaignURL = `${hostURL}/campaigns`;
export const overviewURL = `${hostURL}/overview`;

export const getCampaigns = async () => {
  const res = await fetch(campaignURL, { method: 'GET' });
  const data = await res.json();
  return data;
};

export const getOverviews = async () => {
  const res = await fetch(overviewURL, { method: 'GET' });
  const data = await res.json();
  return data;
};
