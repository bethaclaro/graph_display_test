/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/button-has-type */
import React, { useState } from 'react';
import Campaigns from './containers/Campaigns';
import Create from './containers/Create';
import Overview from './containers/Overview';
import './styles/App.scss';

const App = () => {
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <div className="App">
      <div className="container">
        <ul className="tab tab-block">
          <li className={`tab-item${activeIndex === 0 ? ' active' : ''}`}>
            <a href="#" onClick={() => setActiveIndex(0)}>Overview</a>
          </li>
          <li className={`tab-item${activeIndex === 1 ? ' active' : ''}`}>
            <a href="#" onClick={() => setActiveIndex(1)}>Campaigns</a>
          </li>
          <li className={`tab-item${activeIndex === 2 ? ' active' : ''}`}>
            <a href="#" onClick={() => setActiveIndex(2)}>Create</a>
          </li>
        </ul>
      </div>
      {activeIndex === 0 && (
      <Overview />
      )}
      {activeIndex === 1 && (
      <Campaigns />
      )}
      {activeIndex === 2 && (
      <Create />
      )}
    </div>
  );
};

export default App;
