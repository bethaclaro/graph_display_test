/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';

const Create = () => (
  <div className="container">
    <div className="columns">
      <div className="column col-xs-6">
        <div className="form-group">
          <label className="form-label" htmlFor="campaign-input">Name</label>
          <input className="form-input" type="text" id="campaign-input" placeholder="Input name..." />
          <button className="btn btn-primary" type="submit">Submit</button>
        </div>
      </div>
    </div>
  </div>
);

export default Create;
