/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import {
  CartesianGrid,
  Line, LineChart, XAxis, YAxis,
} from 'recharts';
import { getOverviews } from '../utils/api';

const Overview = (props: any) => {
  const [revenueData, setRevenueData] = useState();
  const [installsData, setInstallsData] = useState();

  const fetchData = async () => {
    const temp = await getOverviews();
    setRevenueData(temp.revenue);
    setInstallsData(temp.installs);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="container">
      <div className="columns">
        <div className="column col-xs-5">
          Installs
          <LineChart width={600} height={300} data={installsData}>
            <Line type="monotone" dataKey="value" stroke="#8884d8" />
            <CartesianGrid stroke="#ccc" />
            <XAxis dataKey="day" />
            <YAxis />
          </LineChart>
        </div>
        <div className="column col-xs-5">
          Revenue
          <LineChart width={600} height={300} data={revenueData}>
            <Line type="monotone" dataKey="value" stroke="#8884d8" />
            <CartesianGrid stroke="#ccc" />
            <XAxis dataKey="day" />
            <YAxis />
          </LineChart>
        </div>
      </div>

    </div>
  );
};

export default Overview;
