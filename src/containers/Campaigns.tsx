/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from 'react';
import {
  CartesianGrid, Line, LineChart, XAxis, YAxis,
} from 'recharts';
import { getCampaigns } from '../utils/api';

const Campaigns = () => {
  const [campaignData, setCampaignData] = useState<any[]>([]);
  const [selectOpts, setSelectOpts] = useState([]);
  const [selectedCampaign, setSelectedCampaign] = useState(1);

  const fetchData = async () => {
    const temp = await getCampaigns();
    setCampaignData(temp);
    const opts = temp.map((item: any) => item.name);
    setSelectOpts(opts);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const onSelectHandler = (ev: any) => {
    const el = document.getElementById('campaign-selector');
  };

  return (
    <div className="container">
      <div className="columns">
        <div className="form-group">
          <select className="form-select" id="campaign-selector" value="2" onChange={onSelectHandler}>
            <option value={0}>Choose an option</option>
            {selectOpts && selectOpts.length > 0 && selectOpts.map((item, index) => (
              <option key={`opt-${item}`} value={index + 1}>{item}</option>
            ))}
          </select>
        </div>
        <div className="column col-xs-1" />
        <div className="column col-xs-10">
          <div className="column col-xs-6">
            <LineChart width={600} height={300} data={campaignData[selectedCampaign + 1]?.installs}>
              <Line type="monotone" dataKey="value" stroke="#8884d8" />
              <CartesianGrid stroke="#ccc" />
              <XAxis dataKey="day" />
              <YAxis />
            </LineChart>
          </div>
        </div>
        <div className="column col-xs-1" />

      </div>
    </div>
  );
};

export default Campaigns;
