import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  active: false,
};

const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setActive: (state, action) => {
      state.active = action.payload;
    },
  },
});

export const { setActive } = appSlice.actions;

export default appSlice.reducer;
